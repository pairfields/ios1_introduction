let lastName = "Бронвальд"
let firstName = "Леонид"
let patronymic = "Алексеевич"
let age = 20
let dateOfBirth = "05.05.2002"
let nativeCity = "Санкт-Петербург"
let university = "ЛЭТИ"
let yearOfStudy = 3
print("Привет, VibeLab!\n" + "Меня зовут: "
        + lastName + " " + firstName + " " + patronymic
        + ", мне \(age) лет.\n"
        + "Я родился и вырос в " + nativeCity + "е, учусь на \(yearOfStudy) курсе " + university + ".")